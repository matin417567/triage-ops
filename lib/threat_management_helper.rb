# frozen_string_literal: true

module ThreatManagementHelper
  THREAT_INSIGHTS_BE    = "Secure:Threat Insights BE Team"
  THREAT_INSIGHTS_FE    = "Secure:Threat Insights FE Team"
  CONTAINER_SECURITY_BE = "Protect:Container Security BE Team"
  CONTAINER_SECURITY_FE = "Protect:Container Security FE Team"

  def threat_insights_be
    @threat_insights_be ||= select_team_member(THREAT_INSIGHTS_BE)
  end

  def threat_insights_fe
    @threat_insights_fe ||= select_team_member(THREAT_INSIGHTS_FE)
  end

  def container_security_be
    @container_security_be ||= select_team_member(CONTAINER_SECURITY_BE)
  end

  def container_security_fe
    @container_security_fe ||= select_team_member(CONTAINER_SECURITY_FE)
  end

  private

  def select_team_member(department)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      memo << "@#{username}" if data['departments']&.any? { |dept| dept == department }
    end.sample
  end
end