# frozen_string_literal: true

require_relative File.expand_path('../lib/group_triage_helper.rb', __dir__)

Gitlab::Triage::Resource::Context.include GroupTriageHelperContext
