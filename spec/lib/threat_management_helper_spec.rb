
# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/threat_management_helper'

RSpec.describe ThreatManagementHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include ThreatManagementHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Secure:Threat Insights BE Team'] },
      'user2' => { 'departments' => ['Secure:Threat Insights FE Team'] },
      'user3' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user4' => { 'departments' => ['Protect:Container Security FE Team'] },
      'user5' => { 'departments' => ['Secure:Threat Insights BE Team'] },
      'user6' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user7' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user8' => { 'departments' => ['Secure:Threat Insights FE Team'] },
      'user9' => { 'departments' => ['Protect:Container Security FE Team'] }
    }
  end

  subject { resource_klass.new(labels) }

  describe '#threat_insights_be' do
    it 'retrieves team members from www-gitlab-com and returns a random threat insights backend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.threat_insights_be).to be_in(%w(@user1 @user5))
    end
  end

  describe '#threat_insights_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random threat insights frontend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.threat_insights_fe).to be_in(%w(@user2 @user8))
    end
  end

  describe '#container_security_be' do
    it 'retrieves team members from www-gitlab-com and returns a random container security backend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.container_security_be).to be_in(%w(@user3 @user6 @user7))
    end
  end

  describe '#container_security_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random container security frontend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.container_security_fe).to be_in(%w(@user4 @user9))
    end
  end
end
